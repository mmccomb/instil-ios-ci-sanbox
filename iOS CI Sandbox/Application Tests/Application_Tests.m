//
//  Application_Tests.m
//  Application Tests
//
//  Created by Matthew McComb on 12/03/2013.
//  Copyright (c) 2013 Matthew McComb. All rights reserved.
//

#import "Application_Tests.h"

@implementation Application_Tests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in Application Tests");
}

@end
