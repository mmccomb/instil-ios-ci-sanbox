//
//  TaskTest.m
//  iOS CI Sandbox
//
//  Created by Matthew McComb on 13/03/2013.
//  Copyright (c) 2013 Matthew McComb. All rights reserved.
//

#import "TaskTest.h"
#import "Task.h"

@implementation TaskTest

- (void)testThatTaskTitleIsMutable {
    const NSString * kTaskTitle = @"Buy milk";
    Task *aTask = [[Task alloc] init];
    [aTask setTitle:(NSString *)kTaskTitle];
    STAssertEqualObjects([aTask title], kTaskTitle, @"Expected task title to be");
}

@end
