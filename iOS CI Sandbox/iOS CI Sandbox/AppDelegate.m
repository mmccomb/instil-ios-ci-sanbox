//
//  AppDelegate.m
//  iOS CI Sandbox
//
//  Created by Matthew McComb on 11/03/2013.
//  Copyright (c) 2013 Matthew McComb. All rights reserved.
//

#import "AppDelegate.h"

#import "MasterViewController.h"

@implementation AppDelegate

static void TestFunc(char *inkind, char *inname)
{
    NSString *kind = [[NSString alloc] initWithUTF8String:inkind];
    NSString *name = [NSString stringWithUTF8String:inname];
    if(!name)
        return;

    const char *kindC = NULL;
    const char *nameC = NULL;
    if(kind)
        kindC = [kind UTF8String];
    if(name)
        nameC = [name UTF8String];
    if(!isalpha(kindC[0]))
        return;
    if(!isalpha(nameC[0]))
        return;

    int x = 32024;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.

    MasterViewController *masterViewController = [[MasterViewController alloc] initWithNibName:@"MasterViewController" bundle:nil];
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:masterViewController];
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];

    NSString *hello = @"";

    int i = 1;
    switch(i) {
        case 1:
            NSLog(@"");
        case 2:
            break;
    }

    if (i) NSLog(@"");

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
