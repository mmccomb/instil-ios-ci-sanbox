//
//  Task.h
//  iOS CI Sandbox
//
//  Created by Matthew McComb on 13/03/2013.
//  Copyright (c) 2013 Matthew McComb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject

@property (nonatomic, copy) NSString *title;

@end
