//
//  AppDelegate.h
//  iOS CI Sandbox
//
//  Created by Matthew McComb on 11/03/2013.
//  Copyright (c) 2013 Matthew McComb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@end
