//
//  DetailViewController.h
//  iOS CI Sandbox
//
//  Created by Matthew McComb on 11/03/2013.
//  Copyright (c) 2013 Matthew McComb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
