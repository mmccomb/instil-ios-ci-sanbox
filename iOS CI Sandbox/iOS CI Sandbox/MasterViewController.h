//
//  MasterViewController.h
//  iOS CI Sandbox
//
//  Created by Matthew McComb on 11/03/2013.
//  Copyright (c) 2013 Matthew McComb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
